
import React from 'react';
import {UseSession} from '../firebase/UserProvider';
import {Route,Redirect} from 'react-router-dom';

export const PrivateRoute =({component :Component,...rest}) =>
{
    const {user}= UseSession();
    return(
        <Route
        {...rest}
          
        render= {(props)=> {
            const id=props.match.params.id;
            
            if(!!user  &&  user.uid ===id)
             {
                return <Component {...props} />; 
            } 
        else{
         return <Redirect to='/login'/>;  
        }
            
        }}
        />
    );
}
