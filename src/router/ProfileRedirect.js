
import React from 'react';
import {UseSession} from '../firebase/UserProvider';
import {Route,Redirect} from 'react-router-dom';

export const ProfileRedirect =({component :Component,...rest}) =>
{
    const {user}= UseSession();
    return(
        <Route
        {...rest}
          
        render= {(props)=> !user ?  (<Component {...props} />) : 
        (
            <Redirect to={
                {
                    pathname:`/DashBoard/${user.uid}`,
                    state:{from: props.location},
                }
            }></Redirect>
        )}
        />
    );
}
