import React from 'react';
import { render } from 'react-dom';
import App from './containers/App';
import './index.css';
import 'bootstrap/dist/css/bootstrap.min.css'; 
import 'react-toastify/dist/ReactToastify.css';
import 'bootstrap/dist/css/bootstrap.min.css'; 
import "@babel/polyfill";
render(
  <App />,
  document.getElementById('root'),
);
