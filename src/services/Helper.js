import { firestore } from '../firebase/config';
import {ErrorMessage} from '../common/Utilities';

export const Status=[
    {
        value:"TODO",
        label:"TO DO"
    },
    {
        value:"Blocked",
        label:"Blocked"
    },
    {
        value:"InProgress",
        label:"In Progress"
    },
    {
        value:"InQA",
        label:"In QA"
    },{
        value:"Done",
        label:"Done"
    },
    {
        value:"Deployed",
        label:"Deployed"
    }];
export const GetStatus = (value) =>
{
    const text=Status.filter(element => element.value===value)[0].label;
 return text;
}

export const GetData = (TID,columnName,TableName,success,error)=>
{
  let dataToFill=[];
  
   firestore.collection(TableName).where(columnName, '==', TID).orderBy('DateTime', 'desc')
  .get()
  .then((querySnapshot) =>{
    
          if(querySnapshot.size > 0)
          {
            querySnapshot.forEach(function(doc) 
            {
              dataToFill.push(doc.data());
            });
            
          }
          success(dataToFill);
          // else{
          //   error("No Record Found !");
          // }
  })
  .catch(function(er) {
    console.log(er);
    error(ErrorMessage);
  });
}

export const GetInformation =  (TableName,success,error) =>
{
  let dataToFill=[];
   firestore.collection(TableName).orderBy('DateTime', 'desc')
  .get()
  .then((querySnapshot) =>{
          if(querySnapshot.size > 0)
          {
            querySnapshot.forEach(function(doc) 
            {
              dataToFill.push(doc.data());
            });
            
          }
          success(dataToFill);
  })
  .catch(function(er) {
    console.log(er);
    error(ErrorMessage);
  });
};


export const GetTaskDetails = (TableName,columnName,Param,success,error) =>{
  
   firestore.collection(TableName).where(columnName, '==', Param)
  .get().then((querySnapshot)=>
   {
    if(querySnapshot.size > 0)
    {
      querySnapshot.forEach(function(doc) 
            {
              success(doc.data());
            });
     
    } else {
      error("No Record Found !");
    }
}).catch(function(er) {
  console.log(er);
  error(ErrorMessage);
});
}