import React, { useState, useEffect,useContext } from 'react';
import { HistoryTask,notification } from '../../common/Utilities';
import {GetData } from '../../services/Helper';
import { TaskContext } from "../../context/ApplicationContext";
import { TableTaskContext } from "../../context/ApplicationContext";
import PopUp from '../template/PopUp';
import { useParams} from "react-router-dom";

function TaskHistory() 
{
  const params = useParams();
    const [context, setContext] = useContext(TaskContext);
    const [Tablecontext, setTableContext] = useState();
    const [isLoading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(true);
    const fetchData = () => {
       GetData(params.TID,"ParentTaskID",HistoryTask,(data)=>
      {
        setTableContext(data);
      },
      (error)=>{
       notification.error(`${error}`);
      });
    };
    fetchData();
    setLoading(false);
    return () => fetchData();
  },[context])

  const formClassName = `ui form ${isLoading ? 'loading' : ''}`;
    return (
        <>
            <div className={formClassName}>
            <TableTaskContext.Provider value={[Tablecontext,setTableContext,true]}>
              <PopUp></PopUp>
            </TableTaskContext.Provider>
    </div>
        </>
    )
}

export default TaskHistory;
