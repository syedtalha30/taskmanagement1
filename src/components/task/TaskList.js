import React, {useState,useEffect,useContext} from 'react'
import {Task,ErrorMessage, notification} from '../../common/Utilities';
import { GetData } from '../../services/Helper';
import { TaskContext } from "../../context/ApplicationContext";
import { TableTaskContext } from "../../context/ApplicationContext";
import PopUp from '../template/PopUp';
import {
  useParams
} from "react-router-dom";


function TaskList() 
{
  const params = useParams();
  const [context] = useContext(TaskContext);
  const [Tablecontext, setTableContext] = useState([]);
  const [isLoading, setLoading] = useState(false);
  

  useEffect(() => {
    setLoading(true);
    GetData(params.PID,"RefProjectID",Task,
      (data)=>
      {
        setTableContext(data);
        setLoading(false);
      },(err)=>{
        notification.error(ErrorMessage);
        setLoading(false);
      });
  }, [context])

  const formClassName = `ui form ${isLoading ? 'loading' : ''}`;
  return (
    <>
    <TableTaskContext.Provider value={[Tablecontext]}>
     <div className={formClassName}>

      <PopUp></PopUp>    
  </div>
  </TableTaskContext.Provider>
    </>
  )
}

export default TaskList;
