import React,{useContext} from 'react';
import { Link,NavLink } from 'react-router-dom';
import {UseSession} from '../../firebase/UserProvider';
import {Dateoptions,DateType} from '../../common/Utilities';
import { GetStatus } from '../../services/Helper';
import { TableTaskContext } from "../../context/ApplicationContext";
 function PopUp()
{
  const [Tablecontext,setTableContext,CheckHistory] = useContext(TableTaskContext);
  
  const {user}= UseSession();
    return (
          <>
          <div className="table-responsive text-nowrap">
            <table className="table table-bordered">
      <thead className="table table-striped table-dark">
        <tr>
          <th>Title</th>
          <th>Description</th>
          <th>Status</th>
          <th>Date & Time</th>
          <th>UserName</th>
        </tr>
      </thead>
      <tbody>
        {(Tablecontext !==undefined  && Tablecontext.length>0) && Tablecontext.map((task) => (
          <tr key={task.Uid}>
            <td>{CheckHistory ? (task.Title) : (<Link to={`/TaskDetails/${user.uid}/${task.Uid}`}>{task.Title}</Link>) }</td>
            <td>{task.Description}</td>            
            <td>{GetStatus(task.Status)}</td>
            <td>{new Intl.DateTimeFormat(DateType, Dateoptions).format(task.DateTime)}
            </td>
            <td>{task.UserName}</td>
          </tr>
        ))}
      </tbody>
    </table>
    </div>
          </>
    )
}

export default PopUp;
