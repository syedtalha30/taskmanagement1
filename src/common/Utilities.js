
import { toast } from 'react-toastify';
export const notification=toast;
export const Task ="task";
export const HistoryTask ="history";
export const Project ="projects";
export const users ="users";
export const SuccessMessage ="Successfully Saved !";
export const ErrorMessage ="Error Occour !";
export const DateType="en-US";

export const Dateoptions={
    year: "numeric",
    month: "long",
    day: "2-digit",
    hour: 'numeric',
    minute: 'numeric',
    second: 'numeric',
   };
