import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import {updateTaskDocument} from '../firebase/storage/Task';
import {UseSession} from '../firebase/UserProvider';
import TaskHistory from '../components/task/TaskHistory';
import { Task,users,notification,ErrorMessage } from '../common/Utilities';
import { GetStatus,Status,GetInformation,GetTaskDetails } from '../services/Helper';
import { TaskContext } from "../context/ApplicationContext";
import { createTaskHistoryDocument } from '../firebase/storage/History';

const TaskDetails = () =>{

     const { register, setValue,errors,clearErrors, handleSubmit } = useForm();
     const [context, setContext] = useState();
     const [getUserVisibility,setUserVisibility]=useState(null);
     const [taskDocument, setTaskDocument] = useState(null);
     const [getusers, setusers] = useState([]);
     const [isLoading, setLoading] = useState(false);
     const {user}= UseSession();
     const params = useParams();
     const PopulateUser = (TableName,TableName1,columnName,TID) =>
     {
        GetInformation(TableName,
       (result)=>
       {
         const UserCollections = result.map((entry) => (
           {
             value: entry.uid,
             label: entry.name,
           }
         ));
         setusers(UserCollections);
         GetDetails(TableName1,columnName,TID);
       },(err)=>
       {
         notification.error(ErrorMessage);
       });
     }

     const GetDetails =  (TableName,ColumnName,param) =>
     {
       GetTaskDetails(TableName,ColumnName,param,
        (documentData)=>
        {
           setTaskDocument(documentData);
           setValue("Title",documentData.Title);
           setValue("Description",documentData.Description);
           setValue("Status",documentData.Status);
           setValue("Users",documentData.UserID);
           const UserVisibility=(!!user && user.displayName===documentData.UserName) ? '':'disabled';
           setUserVisibility(UserVisibility);
           clearErrors();
           setLoading(false);
        },(err)=>
        {
          console.log(err);
          notification.error(ErrorMessage);
          setLoading(false);
        });
     }

    useEffect(() => {
      
         setLoading(true);
         PopulateUser(users,Task,'Uid',params.TID);
        //  GetDetails(Task,'Uid',params.TID)
    }, [context]);

    const onSubmit = data => {
      updateTask(params.TID,data);
      };
      const formClassname = `ui big form twelve wide column ${isLoading ? 'loading' : ''}`;
     
      const updateTask =(TID,data)=>
      {
        setLoading(true);
        const taskDetails={
            RefProjectID:taskDocument.RefProjectID,
            Description: data.Description,
            Title: data.Title,
            Status:data.Status,
            DateTime: Date.now(),
            UserName:getusers.filter((e) => e.value===data.Users)[0].label,
            UserID:data.Users,
            Uid:TID,
        };
        updateTaskDocument(taskDetails,(result)=>
        {
          UpdateHistory(taskDetails,TID);
        },(err)=>{
          console.log(err);
          notification.error(ErrorMessage);
          setLoading(false);
        });
      }
      const UpdateHistory=(data,TID)=>
      {
        createTaskHistoryDocument(
          {
            RefProjectID:data.RefProjectID,
            Description:data.Description,
            Title:data.Title,
            Status:data.Status,
            DateTime:data.DateTime,
            UserName:data.UserName,
            UserID:data.UserID,
            ParentTaskID:TID
          },()=>
          {
            setContext(data);
            setLoading(false);
          },(err)=>{
            notification.error(`${err}`);
            setLoading(false);
          });
      }
      return (
        <>
<TaskContext.Provider value={[context, setContext]}>
<div className="row">
    <div className="col-sm">
    <form className={formClassname} onSubmit={handleSubmit(onSubmit)}>

<label>Title</label>
<input
  type="text"
  name="Title"
  ref={register({ required: true, maxLength: 50 })}
/>
{errors.Title && <p>input is required</p>}

<label>Description</label>
<input
  type="text"
  name="Description"
  ref={register({ required: true })}
/>
{errors.Description && <p>input is required</p>}

<label>Status</label>
<select name="Status" className="DropDownSelect" ref={register({
    required: "Select one Status"})}>
                        {Status.map((option,index) => (
                      <option key={index} value={option.value}>{option.label}</option>
                    ))}
                </select>
                {errors.Status && <p>input is required</p>}
<label>Delegate Users</label>
<select name="Users" className="DropDownSelect" ref={register({
    required: "Select one User"})}>
                        {getusers.map((option,index) => (
                      <option defaultValue={option.value} key={index} value={option.value}>{option.label}</option>
                    ))}
                </select>
                {errors.Users && <p>input is required</p>}
<input type="submit" disabled={getUserVisibility} value="Update" />
</form>
    </div>
    <div className="col-sm-8">
    <h3>History</h3>
    <br></br>
    <TaskHistory></TaskHistory>
</div>
</div>
</TaskContext.Provider>
    </>
    );
}
export default TaskDetails;
