import React,{useState} from 'react'
import List from '../components/project/ProjectList'
import NewProject from '../components/addproject/NewProject'

 import { ProjectContext } from "../context/ApplicationContext";


function DashBoard()
{
    const [context, setContext] = useState({});
    return (
         <ProjectContext.Provider value={[context, setContext]}>
        <div className="container">
          <NewProject></NewProject>
          <List></List>
        </div>
         </ProjectContext.Provider>
      );
}

export default DashBoard;
