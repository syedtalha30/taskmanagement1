import React,{useState} from 'react'
import TaskList from '../components/task/TaskList'
import NewTask from '../components/addtask/NewTask'
import { TaskContext } from "../context/ApplicationContext";

function ProjectTask(props)
 {
    const [context, setContext] = useState();
    return (
        <TaskContext.Provider value={[context, setContext]}>
        <div className="container">
          <NewTask></NewTask>
          <TaskList></TaskList>
        </div>
        </TaskContext.Provider>
      );
}

export default ProjectTask;

