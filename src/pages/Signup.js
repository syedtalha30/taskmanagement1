import React, {useState} from 'react';
import {useForm} from 'react-hook-form';
import { signup } from '../firebase/auth';
import {useHistory} from 'react-router-dom';

function Signup(props) {

    let history = useHistory();
    let newuser;
    const {register , handleSubmit ,errors}= useForm();
    const [isLoading , setLoading] = useState(false);

    const onSubmit = (data) => {
        setLoading(true);
       signup(data,
          (result)=>
          {
            history.push(`/DashBoard/${newuser.uid}`);
          },(err)=>
          {
          });
        setLoading(false);
    };

    const className=`ui form ${isLoading ? 'loading' : ''}`;

  return (
          <form className={className} onSubmit={handleSubmit(onSubmit)}>
            
                <label>
                  First Name</label>
                  <input
                    type="text"
                    name="firstName"
                    placeholder="First Name" ref={register({ required: true })}
                  />
                  {errors.firstName && <p>input is required</p>}
                <label>
                  Last Name</label>
                  <input type="text" name="lastName" placeholder="Last Name" ref={register({ required: true })} />
              <label>
              {errors.lastName && <p>input is required</p>}
                Email </label>
                <input type="email" name="email" placeholder="Email" ref={register({ required: true })} />
                {errors.email && <p>input is required</p>}
              <label>
                Password </label>
                <input type="password" name="password" placeholder="Password" ref={register({ required: true })} />
                {errors.password && <p>input is required</p>}
            <input  type="submit" value="Sign Up"/>
          </form>
  );
}

export default Signup;

