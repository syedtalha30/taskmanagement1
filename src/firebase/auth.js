import firebase from 'firebase';
import { createUserDocument } from './users';
import { notification, SuccessMessage } from '../common/Utilities';

export const signup =  ({
  firstName, lastName, password, email,
}, success, error) => {
  let user;
   firebase.auth().createUserWithEmailAndPassword(email, password)
    .then((data) => {
      user = data.user;
       data.user.updateProfile({ displayName: `${firstName} ${lastName}` })
        .then(async () => {
           createUserDocument(user);
          notification.success(SuccessMessage);
        })
        .catch((err) => {
          console.log(err);
          error(err);
          notification.error(`${err}`);
        });
    }).catch((err) => {
      console.log(err);
      error(err);
      notification.error(`${err}`);
    });

  // notification.error(`${error}`);

  return user;
};

export const logOut = () => firebase.auth().signOut();

export const login = ( ({ email, password }, success, error) => {
   firebase.auth().signInWithEmailAndPassword(email, password)
    .then((data) => {
      success(data.user);
    }).catch((e) => {
      error(e);
    });
});
