
import React, { useState, useEffect, useContext } from 'react';

import firebase from 'firebase';

export const Usercontext = React.createContext();

export const UserProvider = (props) => {
  const [session, setSession] = useState({ User: null, loading: true });
  useEffect(() => {
    const unSubscribe = firebase.auth().onAuthStateChanged((user) => {
      setSession({ user, loading: false });
    });

    return () => unSubscribe();
  }, []);

  return (
    <Usercontext.Provider value={session}>
      {!session.loading && props.children}
    </Usercontext.Provider>
  );
};

export const UseSession = () => {
  const session = useContext(Usercontext);
  return session;
};
