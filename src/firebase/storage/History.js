import { firestore } from '../config';
import { HistoryTask, notification } from '../../common/Utilities';
import { v4 as uuidv4 } from 'uuid';

export const createTaskHistoryDocument =(
    {   RefProjectID,
        Description,
        Title,
        Status,
        DateTime,
        UserName,
        UserID,
        ParentTaskID}
    ,success,error) => 
{
    const id=uuidv4();
    firestore.collection(HistoryTask).doc(id).set({
        RefProjectID,
        Title,
        Description,
        Status,
        UserName,
        DateTime,
        UserID,
        ParentTaskID,
        Uid:id,
    })
    .then(()=> {
        success(id);
       // notification.success(`${SuccessMessage}`);
    })
    .catch((err)=> {
        error(err);
        notification.error(`Task History ${error}`);
    });
  };