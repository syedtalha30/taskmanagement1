import { firestore } from '../config';
import { Project, notification,SuccessMessage } from '../../common/Utilities';
import { v4 as uuidv4 } from 'uuid';

const createProjectDocument = ({PorjectName,DateTime, userName,UserID}) =>
{
    const id=uuidv4();
    firestore.collection(Project).doc(id).set({
        PorjectName: PorjectName,
        DateTime: DateTime,
        userName: userName,
        Uid:id,
        UserID:UserID
    })
    .then(function() {
        notification.success(`${SuccessMessage}`);
    })
    .catch(function(error) {
        notification.error(`${error}`);
    });
};

  export default createProjectDocument;