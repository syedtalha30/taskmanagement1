import { firestore } from './config';
import { users, notification, SuccessMessage } from '../common/Utilities';

export const createUserDocument = (user) => {
  firestore.collection(users).doc(user.uid).set({
    uid: user.uid,
    email: user.email,
    name: user.displayName,
    DateTime: Date.now(),
  })
    .then(() => {
      notification.success(`${SuccessMessage}`);
    })
    .catch((error) => {
      notification.error(`${error}`);
    });
};
